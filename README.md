# Fullstack Developer TEST- Step 1
If there no "vendor folder" in this project you must run
```bash
$ npm install
$ gulp
$ composer install
```
How to run in local environment php server : 
```bash
$ composer serve
```
You can open http://localhost::8888 in your browser